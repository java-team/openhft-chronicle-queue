openhft-chronicle-queue (3.6.0-3) unstable; urgency=medium

  * Team upload.
  * Build-Depends: s/default-jdk/default-jdk-headless/
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Secure URI in copyright format (routine-update)
  * Drop useless get-orig-source target (routine-update)
  * Trim trailing whitespace.
  * Update watch file format version to 4.
  * d/copyright: review

 -- Andreas Tille <tille@debian.org>  Sun, 16 Feb 2025 22:00:09 +0100

openhft-chronicle-queue (3.6.0-2) unstable; urgency=medium

  * Added the missing build dependency on junit4 (Closes: #866566)
  * Standards-Version updated to 4.0.0
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 30 Jun 2017 15:25:14 +0200

openhft-chronicle-queue (3.6.0-1) unstable; urgency=medium

  * New upstream release
    - Build the new openhft-chronicle module
    - Ignore the new chronicle-test module
    - Depend on libopenhft-lang-java (>= 6.6.2)
    - Replaced the koloboke map in WireStorePool with a standard class
    - New dependencies on commons-lang3, hdrhistogram, joda-time,
      chronicle-network and chronicle-wire
  * Depend on libintellij-annotations-java instead of injecting the annotations
  * Build with the DH sequencer instead of CDBS
  * Updated debian/watch to track the 4.x releases
  * Standards-Version updated to 3.9.8 (no changes)
  * Use a secure Vcs-Git URL
  * Remove the jar files from the upstream tarball

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 03 Aug 2016 12:20:48 +0200

openhft-chronicle-queue (2.0.3-1) unstable; urgency=medium

  * Initial release (Closes: #792630)

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 17 Jul 2015 00:06:59 +0200
